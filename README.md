# gtklock-runshell-module
[gtklock](https://github.com/jovanlanik/gtklock) module to add output from a script to the lockscreen.
##
![image](screenshot1.png)
## About
Adds the output of a script to the lockscreen. Markup and refresh are supported.

*** WARNING: ALPHA STATUS. BUT IT WORKS FOR ME. ***

### config file
`command=`<command to be run> NB use 'bash -c $HOME/..' to expand bash variables

`refresh=`\<time in seconds to loop and refresh>

`runshell-position=`[top|center|bottom]-[left|center|right]

`justify=`[left|right|center|fill]

`margin-[top|bottom|left|right]=`\<pixels>

`countdown-after=`\<secs> start a countdown after <secs>

`countdown=`\<secs> number of seconds to countdown.

### config file examples
```
[runshell]
command=bash -c ~/.config/gtklock/timezones.sh
```
or
```
[runshell]
runshell-position=top-right
justify=right
margin-right=10
command=date
refresh=1
countdown=20
countdown-after=350
```
### Sample script
```
#!/usr/bin/env bash
date +'%A, %B %d' # local time
echo "$USER@$HOSTNAME"
acpi -b # battery level
zone=$( date +'%Z' )
if [[ "$zone" =~ AEST.* ]]; then
    alt_zone='GB'
else
    alt_zone='AEST-10'
fi

time=$(TZ="$alt_zone" date +"%H:%M %Z")
echo
echo '<b>'"$time"'</b>' # you can use Pango markup (which will override the CSS)
```
### Sample CSS
To control the appearance of the runshell field, you can
put this in the CSS file with `gtklock --style`:
```
#runshell {
    color: white;
    text-shadow: 1px 1px 2px black;
}
```
##
Based on code from Erik Reider's [gtklock fork](https://github.com/ErikReider/gtklock/tree/user-picture-name).
The `gtklock-module.h` header can be used when making your own modules.

__⚠️ Module version matches the compatible gtklock version. Other versions might or might not work.__
## Dependencies
- GNU Make (build-time)
- pkg-config (build-time)
- gtk+3.0

<!-- 
Local Variables:
mode: conf
eval: (add-hook 'after-save-hook (lambda nil (shell-command "pandoc -s -f gfm --title=README README.md > README.html")) nil 'local)
End:
-->